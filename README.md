# Making and breaking electronic structures: lessons from embedding and  machine learning

Michele's talk at Scuola Normale Superiore on 4/4/2024.

## To fully enjoy it

Make sure to `pip install dftpy qepy`


## Contact

Michele Pavanello
m.pavanello@rutgers.edu
@MikPavanello
